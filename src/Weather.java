/**
 * Created by nmorisi on 4/13/2016.
 */
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
()
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by sierra on 4/11/16.
 */
public class Weather
{
    public final String APIKEY = "ca51a08ea2deeeca";
    JsonElement jse;

    public Weather(String zip)
    {
        getJson(zip);
    }

    public void getJson(String zip)
    {
        String json = "";

        try
        {
            URL url = new URL("http://api.wunderground.com/api/" + APIKEY + "/conditions/q/" + zip + ".json");
            InputStream is = url.openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = rd.readLine()) != null)
            {
                json += line;
            }
            rd.close();
        }
        catch (MalformedURLException mue)
        {
            mue.printStackTrace();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

        JsonParser parser = new JsonParser();
        jse = parser.parse(json);
    }


    public double getTemp()
    {

        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsDouble();

    }

    public String getCity()
    {

        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();

    }

    public String getWeather()
    {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
    }

    public static void main(String[] args)
    {
        Weather w = new Weather("95677");
        double temp = w.getTemp();
        System.out.println("" + temp);
    }
}