import acm.gui.TableLayout;
import acm.program.Program;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by sierra on 4/13/16.
 */
public class GUI extends Program
{
    private JTextField zipField;
    private JTextField tempField;
    private JTextField cityField;
    private JTextField weatherField;

    public GUI()
    {
        start();
    }

    public void init()
    {
        TableLayout layout = new TableLayout(5, 2);
        this.setLayout(layout);

        JLabel zipLabel = new JLabel("Zip");
        zipField = new JTextField("95677");
        Dimension d = new Dimension(100, 30);
        zipField.setPreferredSize(d);
        JButton goButton = new JButton("Go");
        JLabel tempLabel = new JLabel("Temp");
        tempField = new JTextField("");
        tempField.setSize(50, 20);
        JLabel cityLabel = new JLabel("City");
        JLabel weatherLabel = new JLabel("Weather");
        cityField = new JTextField("");
        weatherField = new JTextField("");

        this.add(zipLabel);
        add(zipField);
        add(goButton, "gridwidth=2");
        add(tempLabel);
        add(tempField);
        add(cityLabel);
        add(cityField);
        add(weatherLabel);
        add(weatherField);

        addActionListeners();
        revalidate();
    }

    public static void main(String[] args)
    {
        GUI g = new GUI();
    }

    public void actionPerformed(ActionEvent ae)
    {
        String cmd = ae.getActionCommand();
        if (cmd.equals("Go"))
        {
            Weather w = new Weather(zipField.getText());
            double temp = w.getTemp();
            String weather = w.getWeather();
            String city = w.getCity();
            tempField.setText("" + temp);
            cityField.setText(city);
            weatherField.setText(weather);

        }
    }
}